/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite.category;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.jsondoc.core.annotation.ApiObject;

/**
 *
 * @author yavor
 */
@Entity
@ApiObject
public class Category {
    
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private String id;
    private String categoryName;
    private String categoryDescription;
    
    public Category() {
        
    }
    
    public Category(String categoryId, String categoryName, String categoryDescription) {
        this.id = categoryId;
        this.categoryName = categoryName;
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryId() {
        return id;
    }

    public void setCategoryId(String categoryId) {
        this.id = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    @Override
    public String toString() {
        return "Category{" + "categoryName=" + categoryName + '}';
    }
    
}
