/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite.category;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author yavor
 */
@Service
public class CategoryService {
    
    @Autowired
    private CategoryRepository categoryRepository;
    
    public List<Category> getAllCategories() {
        List<Category> allCategories = new ArrayList<>();
        categoryRepository.findAll().forEach(allCategories::add);
        return allCategories;
    }
    
    public Category getCategory (String id) {
        return categoryRepository.findById(id).orElse(null);
    }
    
    public void addCategory(Category category) {
        categoryRepository.save(category);
    }
    
    public void updateCategory(String id, Category category) {
        categoryRepository.save(category);
    }
    
    public void deleteCategory(String id) {
        categoryRepository.deleteById(id);
    }
}
