/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite.category;

import java.util.List;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author yavor
 */
@RestController
@Api(
        name = "Category API in news site", 
        description = "Manages news categories in a news site", 
        stage = ApiStage.UNDEFINED)

public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @ApiMethod(description = "Gets all the news categories in the site")
    @GetMapping(value = "/category")
    public List<Category> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @ApiMethod(description = "Gets a particular category by category id")
    @GetMapping(value = "/category/{id}")
    public Category getCategory(@ApiPathParam (name = "id") @PathVariable String id) {
        return categoryService.getCategory(id);
    }

    // Admin only
    @ApiMethod(description = "Add a new category available to admin only")
    @PostMapping("/category")
    public void addCategory(@RequestBody Category category) {
        categoryService.addCategory(category);
    }
    
    // Admin only
    @ApiMethod(description = "Updates existing article by id available to admin only")
    @PutMapping("/category/{id}")
    public void updateCategory(@ApiPathParam (name = "id") @PathVariable String id, @RequestBody Category category) {
        categoryService.updateCategory(id, category);
    }
    // Admin only
    @ApiMethod(description = "Deletes a category available to admin only")
    @DeleteMapping("/category/{id}")
    public void deleteCategory(@ApiPathParam (name = "id") @PathVariable String id) {
        categoryService.deleteCategory(id);
    }
}
