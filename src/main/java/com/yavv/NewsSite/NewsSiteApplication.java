package com.yavv.NewsSite;

import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableJSONDoc
public class NewsSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsSiteApplication.class, args);
	}

}
