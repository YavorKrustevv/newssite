/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite.articles;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author yavor
 */
@Service
public class ArticleService {
    
    @Autowired
    private ArticleRepository articleRepository;
    
    public List<Article> getAllArticlesByCategory(String categoryId) {
        List<Article> articles = new ArrayList<>();
        articleRepository.findByCategoryId(categoryId).forEach(articles::add);
        return articles;
    }
    public Article getArticle(String articleId) {
       return articleRepository.findById(articleId).orElse(null);
    }
    
    public void addArticle(Article article) {
        articleRepository.save(article);
    }
    
    public void updateArticle(String articleId, Article article) {
        articleRepository.save(article);
    }
    
    public void deleteArticle(String articleId) {
        articleRepository.deleteById(articleId);
    }
}
