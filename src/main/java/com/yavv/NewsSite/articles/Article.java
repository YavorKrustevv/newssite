/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite.articles;

import com.yavv.NewsSite.category.Category;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

/**
 *
 * @author yavor
 */
@Entity
@ApiObject
public class Article {

    @Id
    @ApiObjectField
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private String id;
    private String articleTitle;
    private String articleAuthor;
    @Temporal(TemporalType.TIMESTAMP)
    private Date articleDate;
    @ManyToOne
    private Category category;

    public Article() {

    }

    public Article(String articleId, String articleTitle, String articleAuthor, Date articleDate, String categoryId) {
        this.id = articleId;
        this.articleTitle = articleTitle;
        this.articleAuthor = articleAuthor;
        this.articleDate = articleDate;
        this.category = new Category(categoryId, "", "");
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getArticleId() {
        return id;
    }

    public void setArticleId(String articleId) {
        this.id = articleId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleAuthor() {
        return articleAuthor;
    }

    public void setArticleAuthor(String articleAuthor) {
        this.articleAuthor = articleAuthor;
    }

    public Date getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(Date articleDate) {
        this.articleDate = articleDate;
    }

    @Override
    public String toString() {
        return "Article{" + "id=" + id + ", articleTitle=" + articleTitle + ", articleAuthor=" + articleAuthor + ", articleDate=" + articleDate + ", category=" + category + '}';
    }

}
