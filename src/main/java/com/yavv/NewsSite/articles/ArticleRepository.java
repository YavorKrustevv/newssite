/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite.articles;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author yavor
 */
public interface ArticleRepository extends JpaRepository<Article, String>{
    
    public List<Article> findByCategoryId(String categoryId);
}
