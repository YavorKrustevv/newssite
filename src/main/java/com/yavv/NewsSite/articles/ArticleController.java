/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite.articles;

import com.yavv.NewsSite.category.Category;
import java.util.Date;
import java.util.List;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author yavor
 */
@RestController
@Api(
    name = "Article API in a news site", 
        description = "Provides methods for article management in a news site",
        stage = ApiStage.UNDEFINED)

public class ArticleController {
    
    @Autowired
    private ArticleService articleService;
    
    @ApiMethod(description = "Lists all the articles by category id")
    @GetMapping("/category/{categoryId}/articles")
    public List<Article> getArticlesByCategory(@ApiPathParam(name = "categoryId") @PathVariable String categoryId) {
        return articleService.getAllArticlesByCategory(categoryId);
    }
    @ApiMethod(description = "Gets a particular article by it's id and the category id")
    @GetMapping("/category/{categoryId}/articles/{articleId}")
    public Article getArticle(@ApiPathParam (name = "categoryId,articleId") @PathVariable String categoryId, @PathVariable String articleId) {
        return articleService.getArticle(articleId); 
    }
    // Admin only
    @ApiMethod(description = "Method for adding a new article under particular category, available to admin only")
    @PostMapping("/category/{categoryId}/articles")
    public void addArticle(@ApiPathParam (name = "categoryId") @PathVariable String categoryId, @RequestBody Article article) {
        article.setCategory(new Category(categoryId, "", ""));
        article.setArticleDate(new Date());
        articleService.addArticle(article);
    }
    // Admin only
    @ApiMethod(description = "Method for updating a particular articles by it's id in a particular category, available to admin only")
    @PutMapping("/category/{categoryId}/articles/{articleId}")
    public void updateArticle(@ApiPathParam (name = "categoryId,articleId") @PathVariable String categoryId, @PathVariable String articleId, @RequestBody Article article) {
        article.setCategory(new Category(categoryId, "", ""));
        articleService.updateArticle(articleId, article);
    }
    // Admin only
    @ApiMethod(description = "Method for deleting an article by id in a particular category, available to admin only")
    @DeleteMapping("/category/{categoryId}/articles/{articleId}")
    public void deleteArticle(@ApiPathParam (name = "categoryId,articleId") @PathVariable String articleId) {
        articleService.deleteArticle(articleId);
    }
    
}
