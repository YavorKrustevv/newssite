/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.NewsSite;

import com.yavv.NewsSite.articles.Article;
import com.yavv.NewsSite.articles.ArticleRepository;
import com.yavv.NewsSite.category.Category;
import com.yavv.NewsSite.category.CategoryRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author yavor
 */
@Component
public class DatabaseSeeder implements CommandLineRunner {
    
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public void run(String... args) throws Exception {
        
        List<Category> categories = new ArrayList<>();
        List<Article> articles = new ArrayList<>();
        
        categories.add(new Category("worldnews", "World News", "news from the globe"));
        categories.add(new Category("bg", "Bulgaria", "news from the country"));
        categories.add(new Category("tech", "Technology", "technology news"));
        
        categoryRepository.saveAll(categories);
                
        articles.add(new Article("falcon-heavy1","Falcon Heavy lift off","Georgy Ivanov",new Date(),"worldnews"));
        articles.add(new Article("pope-is-here", "The Pope arrives today","Hristina Pavlova",new Date(),"bg"));
        articles.add(new Article("foldablePhone","Samsung fold hiccups","Milen Koev",new Date(),"tech"));
        
        articleRepository.saveAll(articles);
    }
    
}
