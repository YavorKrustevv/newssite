
This is a basic barebones structure I created using Spring Boot.
I decided on Derby database as it's in memory and it's easier for prototyping, at least for me.
The command runner class proved to be useful as I wanted to input some data every time and 
to be flexible on changing it with each restart.